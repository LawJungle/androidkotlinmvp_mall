package com.wyj.base.presenter

import com.wyj.base.presenter.view.BaseView

/*
    MVP中P层 基类
 */
open class BasePresenter<T : BaseView> {
    // P 层 持有 接口 对象
    lateinit var mView: T

    //Dagger注入，Rx生命周期管理
//    @Inject
//    lateinit var lifecycleProvider: LifecycleProvider<*>


//    @Inject
//    lateinit var context: Context

    /*
        检查网络是否可用
     */
//    fun checkNetWork(): Boolean {
//        if (NetWorkUtils.isNetWorkAvailable(context)) {
//            return true
//        }
//        mView.onError("网络不可用")
//        return false
//    }
}
