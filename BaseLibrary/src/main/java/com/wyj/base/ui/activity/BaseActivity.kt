package com.wyj.base.ui.activity

import android.support.v7.app.AppCompatActivity

/**
 *@author 王永吉
 *@date 2018/10/19 14:31
 *
 */
open class BaseActivity : AppCompatActivity() {
}