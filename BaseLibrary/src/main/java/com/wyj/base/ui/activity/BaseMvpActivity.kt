package com.wyj.base.ui.activity

import com.wyj.base.presenter.BasePresenter
import com.wyj.base.presenter.view.BaseView

/**
 *@author 王永吉
 *@date 2018/10/19 14:31
 *
 */
open class BaseMvpActivity<T : BasePresenter<*>> : BaseActivity(), BaseView {
    //Activity  层  持有 P对象
    //Activity  层  实现 接口回调

    lateinit var mPresenter: T

    override fun hideLoading() {
    }

    override fun showLoading() {
    }

    override fun onError() {
    }

}