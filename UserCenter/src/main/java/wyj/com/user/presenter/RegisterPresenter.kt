package wyj.com.user.presenter

import com.kotlin.user.service.impl.UserServiceImpl
import com.wyj.base.presenter.BasePresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.AnkoLogger
import org.reactivestreams.Subscriber
import org.reactivestreams.Subscription
import wyj.com.user.presenter.view.RegisterView
import kotlin.math.log

/*
    MVP中P层 基类
    // P 层 持有 接口 对象
 */
open class RegisterPresenter : BasePresenter<RegisterView>() {

    fun register(mobile: String, pwd: String, verifyCode: String) {
        //TODO 处理业务逻辑
        //TODO 执行视图回调
        val userService = UserServiceImpl()
        userService.register(mobile, pwd, verifyCode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Subscriber<Boolean> {
                    override fun onSubscribe(p0: Subscription?) {
                        mView.onRegisterResult("onSubscribe");
                        println("onSubscribe---> ${Thread.currentThread().name} ---> ${Thread.currentThread().id}   ")
                    }

                    override fun onComplete() {
                        println("onComplete")
                        mView.onRegisterResult("onComplete");
                    }

                    override fun onNext(p0: Boolean?) {
                        mView.onRegisterResult("onNext");
                        println("onNext")
                    }

                    override fun onError(p0: Throwable?) {
                        mView.onRegisterResult("onError");
                        println("onError")
                    }

                })

    }

}
