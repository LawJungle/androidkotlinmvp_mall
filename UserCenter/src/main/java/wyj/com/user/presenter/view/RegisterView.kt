package wyj.com.user.presenter.view

import com.wyj.base.presenter.view.BaseView


/**
 *MVP中视图回调 基类
 */
interface RegisterView : BaseView {
    fun onRegisterResult(result: String)
}
