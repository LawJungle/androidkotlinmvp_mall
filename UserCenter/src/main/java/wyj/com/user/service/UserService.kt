package com.kotlin.user.service

import io.reactivex.Flowable
import io.reactivex.Observable


/*
    用户模块 业务接口
 */
interface UserService {

    //用户注册
    fun register(mobile: String, pwd: String, verifyCode: String): Flowable<Boolean>

    //用户登录
    fun login(mobile: String, pwd: String, pushId: String)

    //忘记密码
    fun forgetPwd(mobile: String, verifyCode: String)

    //重置密码
    fun resetPwd(mobile: String, pwd: String)

    //编辑用户资料
    fun editUser(userIcon: String, userName: String, userGender: String, userSign: String)
}
