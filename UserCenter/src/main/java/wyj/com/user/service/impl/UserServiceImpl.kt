package com.kotlin.user.service.impl

import com.kotlin.user.service.UserService
import io.reactivex.Flowable
import io.reactivex.Observable


/*
    用户模块业务实现类
 */
class UserServiceImpl : UserService {
//    @Inject
//    @Inject
//    lateinit var repository: UserRepository

    /*
        注册
     */
    override fun register(mobile: String, pwd: String, verifyCode: String): Flowable<Boolean> {
        return Flowable.just(true)
//        return repository.register(mobile, pwd, verifyCode).convertBoolean()
    }

    /*
        登录
     */
    override fun login(mobile: String, pwd: String, pushId: String) {
//        return repository.login(mobile, pwd, pushId).convert()
    }

    /*
        忘记密码
     */
    override fun forgetPwd(mobile: String, verifyCode: String) {
//        return repository.forgetPwd(mobile, verifyCode).convertBoolean()
    }

    /*
        重置密码
     */
    override fun resetPwd(mobile: String, pwd: String) {
//        return repository.resetPwd(mobile, pwd).convertBoolean()
    }

    /*
        修改用户资料
     */
    override fun editUser(userIcon: String, userName: String, userGender: String, userSign: String) {
//        return repository.editUser(userIcon, userName, userGender, userSign).convert()
    }

}
