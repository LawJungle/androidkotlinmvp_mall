package wyj.com.user.ui.activity

import android.os.Bundle
import wyj.com.user.presenter.RegisterPresenter
import wyj.com.user.presenter.view.RegisterView
import com.wyj.base.ui.activity.BaseMvpActivity
import kotlinx.android.synthetic.main.activity_register.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import wyj.com.user.R


/**
 * 注册界面
 */
class RegisterActivity : BaseMvpActivity<RegisterPresenter>(), RegisterView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        mPresenter = RegisterPresenter()
        mPresenter.mView = this;
        mRegisterBtn.setOnClickListener {
            //            mPresenter.register(applicationContext, "", "", "");
            mPresenter.register("", "", "");
//            startActivity<RegisterActivity2>("1" to 20, "2" to 20)
        }
    }

    override fun onRegisterResult(result: String) {
        toast(result)
    }

    override fun showLoading() {
        toast("zhuUI")
    }
}
